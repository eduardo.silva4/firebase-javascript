// Such info must be obtained from Firebase Project, on firebase console
var firebaseConfig = {
  apiKey: "THE-PROJECT-API-KEY",
  authDomain: "THE-AUTH-DOMAIN.firebaseapp.com",
  databaseURL: "https://THE-DATABASE-URL.firebaseio.com",
  projectId: "THE-PROJECT-ID",
  storageBucket: "THE-STORAGE-BUCKET.appspot.com",
  messagingSenderId: "THE-MESSAGING-SENDER-ID",
  appId: "THE:API:WEB:ID",
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)
